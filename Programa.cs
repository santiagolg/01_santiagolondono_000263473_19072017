﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppCursos
{
    class Programa
    {

        static List<Estudiante> estudiantes = new List<Estudiante>();
        static List<Curso> cursos = new List<Curso>();

        static void Main(string[] args)
        {
            Console.WriteLine("Menu:");
            Menu();
            //Este comentario lo hizo Juan Pablo Giraldo
        }

        public static void Menu()
        {
            int opcion = 0;

            while(opcion != 5)
            {
                Console.WriteLine("Menu");
                Console.WriteLine("1.Crear Estudiante\n2.Crear Curso\n3.Inscribir Estudiante\n4.Mostrar Curso\n5.Salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        CrearEstudiante();
                        break;
                    case 2:
                        CrearCurso();
                        break;
                    case 3:
                        InscribirEstudiante();
                        break;
                    case 4:
                        MostrarCurso();
                        break;
                    default:
                        opcion = 5;
                        break;
                }
            }
        }

        private static void MostrarCurso()
        {
            Console.WriteLine("Ingrese el nombre del curso:");
            string nombreCurso = Console.ReadLine();
            for (int i = 0; i < cursos.Count; i++)
            {
                if (cursos[i].Nombre == nombreCurso)
                {
                    cursos[i].MostrarCurso();
                }
            }
        }

        private static void InscribirEstudiante()
        {
            Console.WriteLine("Ingrese la cedula del estudiante:");
            string cedula = Console.ReadLine();
            Console.WriteLine("Ingrese el nombre del curso:");
            string nombreCurso = Console.ReadLine();
            Estudiante estudiante = new Estudiante();
            for(int j = 0; j < estudiantes.Count; j++)
            {
                if(estudiantes[j].Cedula == cedula)
                {
                    estudiante = estudiantes[j];
                }
            }
            for (int i = 0; i < cursos.Count; i++)
            {
                if (cursos[i].Nombre == nombreCurso)
                {
                    cursos[i].AgregarEstudiante(estudiante);
                }
            }
        }

        public static void CrearEstudiante()
        {
            Console.WriteLine("Ingrese el nombre del estudiante:");
            string nombreEstudiante = Console.ReadLine();
            Console.WriteLine("Ingrese la cedula del estudiante:");
            string cedula = Console.ReadLine();
            estudiantes.Add(new Estudiante(nombreEstudiante, cedula));
        }

        private static void CrearCurso()
        {
            Console.WriteLine("Ingrese el nombre del curso:");
            string nombreCurso = Console.ReadLine();
            Console.WriteLine("Ingrese el día del curso:");
            string diaCurso = Console.ReadLine();
            Console.WriteLine("Ingrese la hora del curso:");
            string horaCurso = Console.ReadLine();
            Console.WriteLine("Ingrese el lugar del curso:");
            string lugarCurso = Console.ReadLine();
            Console.WriteLine("Ingrese el nombre del profesor que dictará el curso:");
            string profesor = Console.ReadLine();
            cursos.Add(new Curso(nombreCurso,diaCurso,horaCurso,profesor,lugarCurso));
        }
    }
}
