﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppCursos
{
    class Curso
    {
        private string nombre;
        private string diaClase;
        private string horaClase;
        private string profesor;
        private string lugar;
        private List<Estudiante> estudiantes = new List<Estudiante>();

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string DiaClase
        {
            get
            {
                return diaClase;
            }

            set
            {
                diaClase = value;
            }
        }

        public string HoraClase
        {
            get
            {
                return horaClase;
            }

            set
            {
                horaClase = value;
            }
        }

        public string Profesor
        {
            get
            {
                return profesor;
            }

            set
            {
                profesor = value;
            }
        }

        public string Lugar
        {
            get
            {
                return lugar;
            }

            set
            {
                lugar = value;
            }
        }

        internal List<Estudiante> Estudiantes
        {
            get
            {
                return estudiantes;
            }

            set
            {
                estudiantes = value;
            }
        }

        public Curso(string nombre, string diaClase, string horaClase, string profesor, string lugar)
        {
            this.nombre = nombre;
            this.diaClase = diaClase;
            this.horaClase = horaClase;
            this.profesor = profesor;
            this.lugar = lugar;
        }
        
        public Curso()
        {
            this.nombre = "";
            this.diaClase = "";
            this.horaClase = "";
            this.profesor = "";
            this.lugar = "";
        }

        public void AgregarEstudiante(Estudiante estudiante)
        {
            this.estudiantes.Add(estudiante);
        }

        public void MostrarCurso()
        {
            Console.WriteLine("Nombre del curso: {0}", this.nombre);
            Console.WriteLine("Dia del curso: {0}", this.diaClase);
            Console.WriteLine("Hora del curso: {0}", this.horaClase);
            Console.WriteLine("Lugar del curso: {0}", this.lugar);
            Console.WriteLine("Profesor que dicta del curso: {0}", this.profesor);
            Console.WriteLine("Estudiantes inscritos al curso:");
            foreach(Estudiante actual in estudiantes)
            {
                Console.WriteLine(actual.Nombre);
            }
        }
    }
}
