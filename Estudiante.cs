﻿namespace AppCursos
{

    class Estudiante
    {

        private string nombre;
        private string cedula;

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Cedula
        {
            get
            {
                return cedula;
            }

            set
            {
                cedula = value;
            }
        }

        public Estudiante(string nombre, string cedula)
        {
            this.nombre = nombre;
            this.cedula = cedula;
        }
        
        public Estudiante()
        {
            this.nombre = "";
            this.cedula = "";
        }
       
    }
}
